f=open("ping.txt", "r", encoding="UTF-8")
min_time=10000
max_time=0
avg_time=0
counter_ping=0
counter_lost_ping=0
for s in f.readlines():
    counter_ping+=1
    if s=="Request timed out\n":
        counter_lost_ping+=1
    else:
        s_list=s.split()
        t=int(s_list[4][5:-2])
    if min_time>t:
        min_time=t
    if t>max_time:
        max_time=t
avg_time=(t+t+t+t+t)//5
received=counter_ping - counter_lost_ping
print(counter_ping, " packets transmitted,", received, " packets received,", counter_lost_ping, " packets lost,", "Minimum=",min_time,"ms,", "Maximum=",max_time,"ms,",  "Average=",avg_time,"ms")

