import pygame as pg

class SpaceObject(pg.sprite.Sprite):
    def __init__(self, position, image, size, master_object, arbyte_speed, self_speed, radius):
        super().__init__()
        self.base_image = image
        self.base_image = pg.transform.scale(self.base_image, size)
        
        self.image = self.base_image.copy()
        self.rect = self.base_image.get_rect()        
        
        self.rect.center = position
        
        self.self_rotation_angle_speed = self_speed
        self.self_current_angle = 0
        
        self.arbyte_rotation_angle_speed = arbyte_speed
        self.current_arbyte_angle = 0
        
        self.master_object = master_object
        
        self.radius = radius
                
    def update(self, *args):
        time_delta = args[0]
        
        angle = self.self_rotation_angle_speed * time_delta
        self.self_current_angle += angle 
        self.self_current_angle %= 360
        
        arbyte_angle = self.arbyte_rotation_angle_speed * time_delta
        self.current_arbyte_angle += arbyte_angle
        self.current_arbyte_angle %= 360
        
        self.image, self.rect = self.rotate()
        
    def rotate(self):
        rotated_image = pg.transform.rotate(self.base_image, self.self_current_angle)
        rotated_rect = rotated_image.get_rect()
        
        
        if self.master_object is not None:
            arbyte_vector = pg.Vector2(0, self.radius)
            position = arbyte_vector.rotate(self.current_arbyte_angle) + self.master_object.rect.center
        else:
            position = self.rect.center
        
        rotated_rect.center = position
        return rotated_image, rotated_rect


pg.init()

window = pg.display.set_mode((1000, 800))
back = pg.transform.scale(pg.image.load('./images/back.jpg'), (1000, 800))

clock = pg.time.Clock()
all_objects = pg.sprite.Group()

sun = SpaceObject(
    position=window.get_rect().center, 
    image=pg.image.load('./images/sun.png'), 
    size=(200, 200),
    arbyte_speed=0, 
    self_speed=0.1,
    master_object=None,
    radius=0
)

earth = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/earth.png'), 
    size=(50, 50),
    master_object=sun, 
    arbyte_speed=-0.06, 
    self_speed=0.5,
    radius=180
)

moon = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/moon.png'), 
    size=(15, 15),
    master_object=earth, 
    arbyte_speed=-0.1, 
    self_speed=0.8,
    radius=50
)

mercury = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/mercury.png'), 
    size=(20, 20),
    master_object=sun, 
    arbyte_speed=-0.026, 
    self_speed=0.1,
    radius=100
)

vinus = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/vinus.png'), 
    size=(35, 35),
    master_object=sun, 
    arbyte_speed=-0.05, 
    self_speed=-0.1,
    radius=130
)

mars = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/mars.png'), 
    size=(50, 50),
    master_object=sun, 
    arbyte_speed=-0.01, 
    self_speed=0.5,
    radius=240
)

jupiter = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/jupiter.png'), 
    size=(65, 65),
    master_object=sun, 
    arbyte_speed=-0.015, 
    self_speed=0.5,
    radius=320
)

saturn = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/saturn.png'), 
    size=(65, 65),
    master_object=sun, 
    arbyte_speed=-0.03, 
    self_speed=0.5,
    radius=390
)

uran = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/uran.png'), 
    size=(55, 55),
    master_object=sun, 
    arbyte_speed=-0.05, 
    self_speed=-0.5,
    radius=450
)

neptun = SpaceObject(
    position=(0, 0), 
    image=pg.image.load('./images/neptun.png'), 
    size=(55, 55),
    master_object=sun, 
    arbyte_speed=-0.04, 
    self_speed=0.5,
    radius=510
)
all_objects.add((sun, earth, moon, mercury, vinus, mars, jupiter, saturn, uran, neptun))

is_running = True
while is_running:
    timedelta = clock.tick(30)
    # обработка событий
    for event in pg.event.get():
        if event.type == pg.QUIT:
            is_running = False


    all_objects.update(timedelta)
    
    window.blit(back, (0, 0))
    all_objects.draw(window)
    
    pg.display.flip()
    
pg.quit()