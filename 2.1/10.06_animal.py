from threading import Timer



class Animal:
    def __init__(self):
        self.well_fed = 100
        self.happy = 100
        self.is_alive = True
        self.timer = Timer(2, self.live)
        self.timer.start()
        
    def live(self):
        if(self.happy == 0 or self.well_fed == 0):
            self.is_alive = False
        else:
            self.timer.cancel()
            self.timer = Timer(4, self.live)
            self.timer.start()
            self.well_fed -= 10
            self.happy -= 10
            print(a)
        
        
    def feed(self):        
        self.well_fed += 20
        
    def play(self):
        self.happy += 10
        
    def __str__(self):
        return f'well_fed: {self.well_fed}, Happy: {self.happy}'
    
a = Animal()
while a.is_alive:
    command = input()
    match command:
        case 'feed':
            a.feed()
        case 'play':
            a.play()
        case 'quit':
            a.timer.cancel()
            a.is_alive = False
        case 'current':
            print(a)
        case _:
            print('Unknown command')
            
print('You lose')