class My_list(list):
    
    def __init__(self, *items):
        super().__init__(items)
        
    def __truediv__(self, other):
        other = My_list(other)
        
        ans = My_list()
        ans.extend(self)
        ans.extend(other)
        return ans
    
    def __sub__(self, other):
        ans = My_list()
        ans.append(self)
        ans.append(other)        
        return ans
    
    def __str__(self):
        string = "My_list: \n" 
        string += "length: " + str(len(self)) + "\n"
        string += "items: " + super().__str__() + "\n"
        return string

# myList = MyList([1,2,3,4,5])
# myList2 = MyList(range(1, 100, 1))
# print(myList / myList2)
# print(MyList.__truediv__(myList, myList2))
# print(myList - myList2)
# print(myList)
l1 = My_list(1, 2, 3)
l2 = My_list(3, 4, 5)
l3 = l2 - l1
l4 = l1 - l2
print(l1 / 5)
print(l3)
print(l4)
