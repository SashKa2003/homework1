import random 
import time
import tkinter as tk
def task1():
    a=10
    b=12
    c=4
    a,b,c=b,c,a
    print(a,b,c)
    content()

def task2():
    correct_input=False
    while not correct_input:
        try:
            a=int(input('введите число для суммирования '))
            b=int(input('введите число для суммирования '))
            correct_input=True
        except ValueError:
            print("введено не число")
    print('a+b= ',a+b)
    content()

def task2_2():
    n=int(input('введите количество чисел '))
    r=0
    for i in range(0,n):
        correct_input=False
        while not correct_input:
            try:
                a=int(input('введите число для суммирования '))
                correct_input=True
                r+=a
            except ValueError:
                print("введено не число")
    print(r)
    content()

def task3():
    x=random.randint(0,100)
    print("x=",x, "x^5=",x**5)
    time_elapsed = time.process_time()
    print(time_elapsed)
    content()

def task3_1():
    x=random.randint(0,100)
    print("x=",x, "x^5=",x*x*x*x*x)
    time_elapsed = time.process_time()
    print(time_elapsed)
    content()

def task4():
    a = int(input('введите число от 0 до 250: '))
    x1 = 0
    x2 = 1
    while True:
        if x2 <= a:
            if x2 == a:
                print(a, 'является числом Фибоначчи')
                break
            else:
                x1, x2 = x2, x1 + x2
        else:
            print(a, 'не является числом Фибонначи')
            break
    content()

def task5_1():
    a = int(input('введите порядок месяца: '))

    if a == 1 or a == 2 or a == 12:
        print('Зима')
    elif a == 3 or a == 4 or a == 5:
        print('Весна')
    elif a == 6 or a == 7 or a== 8:
        print('Лето')
    else:
        print('Осень')
    content()

def task5_2():
    seasons = {'Зима': (1, 2, 12), 'Весна': (3, 4, 5), 'Лето': (6, 7, 8), 'Осень': (9, 10, 11)}
    m = int(input('Введите порядок месяца: '))
    for key in seasons:
        if m in seasons[key]:
            print(key)
            content()

def task6():
    N = int(input('введите количество чисел: '))
    count = 1
    s = 0
    c = 0
    alln = 0
    allh = 0
    while count <= N:
        if count %2 == 0:
            allh += 1
            s = s + count
            count = count + 1 
        else:
            alln += 1
            c = c + count
            count = count + 1
    print('сумма всех четных чисел = ', s)
    print('сумма всех нечетных чисел = ', c)
    print('колличество четных чисел = ', allh)
    print('количество нечетных чисел = ', alln)
    content()

def task7():
        N = int(input('введите границу промежутка: '))
        for i in range(1, N):
            count = 0
            for j in range(1, N):
                if i%j == 0:
                    count += 1
            print (i, count)
        content()

def task8():
    N = int(input('введите начало интервала: '))
    M = int(input('введите конец интервала: '))
    a = []
    for i in range(N, M):
        for j in range(N, i):
            for k in range(N, j):
                if i**2 == j**2+k**2: 
                    a1 = [i,j,k]
                    a.append(a1)
    print(a)
    content()

def task9():
    N = int(input('введите начало интервала: '))
    M = int(input('введите конец интервала: '))
    for i in range(N,M):
        s = True
        k = str(i)
        for j in range(0, len(k)):
            if k[j] != "0":
                if i%int(k[j]) != 0:
                    s = False
                    break
        if s:
            print(i)
    content()

def task10():
    a = int(input('Укажите границу проверки совершенных чисел: '))
    n = 0
    for i in range(0, a):
        s = 1
        for j in range(2, i // 2 + 1):
            if i % j == 0:
                s += j
        if s == i:
            n += 1
            if n < 5:
                print(i)
    content() 

def task11():
    arr = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']
    print(arr)
    print('Последний элемент массива: ', arr[-1])
    print('Последний элемент массива: ', str(next(reversed(arr))))
    print('Последний элемент массива: ', arr.pop())
    content() 

def task12():
    arr = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']
    arr.reverse()
    print(arr)
    content() 

def rec_summa(a, n):
        if n <= 0:
            return 0
        else:
            return sum(a, n - 1) + a[n - 1]
def task13():
    a = [10, 2, 6, 23, 1]
    print("Сумма: ", rec_summa(a, len(a)))
    content() 

def task14_1():
    def converter():
        try:
            rubles = float(rubles_entry.get())
            dollars = rubles / exchange_rate  
            result_label.config(text=f"Сумма в долларах: ${dollars:.2f}")
        except ValueError:
            result_label.config(text="Введено неправильное число")
    window = tk.Tk()
    window.title("Конвертер рублей в доллары")

    exchange_rate = 97.0018

    rubles_label = tk.Label(window, text="Введите сумму в рублях:")
    rubles_label.pack()

    rubles_entry = tk.Entry(window)
    rubles_entry.pack()

    convert_button = tk.Button(window, text="Конвертировать", command=converter)
    convert_button.pack()

    result_label = tk.Label(window, text="")
    result_label.pack()

    window.mainloop()
    content() 

def task14_2():
    def converter():
        try:
            amount = float(amount_entry.get())
            if from_currency.get() == "Рубли в Доллары":
                converted_amount = amount / exchange_rate  
                result_label.config(text=f"Сумма в долларах: ${converted_amount:.2f}")
            elif from_currency.get() == "Доллары в Рубли":
                converted_amount = amount * exchange_rate 
                result_label.config(text=f"Сумма в рублях: {converted_amount:.2f}")
        except ValueError:
            result_label.config(text="Введено неправильное число")

    window = tk.Tk()
    window.title("Конвертер валют")

    exchange_rate = 97.0018

    currency_label = tk.Label(window, text="Выберите из какой валюты в какую выполнить конвертацию:")
    currency_label.pack()

    from_currency = tk.StringVar()
    from_currency.set("Рубли в Доллары") 
    currency_option = tk.OptionMenu(window, from_currency, "Рубли в Доллары", "Доллары в Рубли")
    currency_option.pack()

    amount_label = tk.Label(window, text="Введите сумму:")
    amount_label.pack()

    amount_entry = tk.Entry(window)
    amount_entry.pack()

    convert_button = tk.Button(window, text="Конвертировать", command=converter)
    convert_button.pack()

    result_label = tk.Label(window, text="")
    result_label.pack()

    window.mainloop()
    content() 

def task15():
    N = int(input("Введите высоту таблицы: "))
    M = int(input('Введите ширину таблицы: '))
    if N < 5 or N > 20 or M < 5 or M > 20:
        print("Размеры таблицы от 5 до 20")
    else:
        for i in range(1, N + 1):
            for j in range(1, M + 1):
                print("%4d" % (i * j), end="")
            print()
        content() 

def content(): 
    print("Введите номер задания в числовом виде (пример: 1, 2.1, 5.2)")
    task = float(input())
    match task:
            case 1.0:
                task1()
            case 2.1:
                task2()
            case 2.2:
                task2_2()
            case 3.1:
                task3()
            case 3.2:
                task3_1()
            case 4.0:
                task4()
            case 5.1:
                task5_1()
            case 5.2:
                task5_2()
            case 6.0:
                task6()
            case 7.0:
                task7()
            case 8.0:
                task8()
            case 9.0:
                task9()
            case 10.0:
                task10()
            case 11.0:
                task11()
            case 12.0:
                task12()
            case 13.0:
                task13()
            case 14.1:
                task14_1()
            case 14.2:
                task14_2()
            case 15.0:
                task15()
            case _:
                print("такого номера нет")
                content()
content() 













