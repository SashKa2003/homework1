import sys
import numpy as np
import pygame
import math


class Board:
    def __init__(self, columns, rows):
        self.columns = columns
        self.rows = rows
        self.board = np.zeros((columns, rows))

    def get_board(self):
        return self.board

    def draw_board(self, screen, square_size, height):
        for col in range(columns_count):
            for row in range(rows_count):
                pygame.draw.rect(screen, grey, (col * square_size, row * square_size + square_size, square_size, square_size))
                pygame.draw.circle(screen, white, (int(col * square_size + square_size / 2), int(row * square_size + square_size + square_size / 2)), int(square_size / 2 - 5))

        for col in range(columns_count):
            for row in range(rows_count):
                if self.board[row][col] == 1:
                    pygame.draw.circle(screen, red, (int(col * square_size + square_size / 2), height - int(row * square_size + square_size / 2)), int(square_size / 2 - 5))
                elif self.board[row][col] == 2:
                    pygame.draw.circle(screen, yellow, (int(col * square_size + square_size / 2), height - int(row * square_size + square_size / 2)), int(square_size / 2 - 5))
        pygame.display.update()


class Player:
    def __init__(self, number, color):
        self.number = number
        self.color = color

    def make_move(self, event, screen):
        mouse_x_position = event.pos[0]
        column = int(math.floor(mouse_x_position / app.square_size))

        if game_system.is_valid_location(column):
            row = game_system.get_next_open_row(column)
            game_system.drop_cell(row, column, self.number)
            game_system.check_win(self.number, self.color, screen)


class Game:
    def __init__(self, board):
        self.over = False
        self.turn = 0
        self.board = board.get_board()

    def drop_cell(self, row, col, cell):
        self.board[row][col] = cell

    def is_valid_location(self, col):
        if self.board[5][col] == 0:
            return True
        else:
            return False

    def get_next_open_row(self, col):
        for row in range(rows_count):
            if self.board[row][col] == 0:
                return row

    def check_horizontal_win(self, cell):
        for col in range(columns_count - 3):
            for row in range(rows_count):
                if self.board[row][col] == cell and self.board[row][col + 1] == cell \
                        and self.board[row][col + 2] == cell and self.board[row][col + 3] == cell:
                    return True

    def check_vertical_win(self, cell):
        for col in range(columns_count):
            for row in range(rows_count - 3):
                if self.board[row][col] == cell and self.board[row + 1][col] == cell \
                        and self.board[row + 2][col] == cell and self.board[row + 3][col] == cell:
                    return True

    def check_diagonal_win(self, cell):
        for col in range(columns_count - 3):
            for row in range(rows_count - 3):
                if self.board[row][col] == cell and self.board[row + 1][col + 1] == cell \
                        and self.board[row + 2][col + 2] == cell and self.board[row + 3][col + 3] == cell:
                    return True
        for col in range(columns_count - 3):
            for row in range(rows_count - 3, rows_count):
                if self.board[row][col] == cell and self.board[row - 1][col + 1] == cell \
                        and self.board[row - 2][col + 2] == cell and self.board[row - 3][col + 3] == cell:
                    return True

    def check_win(self, number, color, screen):
        my_font = pygame.font.SysFont("Arial", 45, bold=True)
        if self.check_vertical_win(number) or self.check_horizontal_win(number) \
                or self.check_diagonal_win(number):
            label = my_font.render("Игрок " + str(number) + " победил!", 1, color)
            screen.blit(label, (50, 15))
            self.over = True
        elif all(all(item != 0 for item in row) for row in self.board):
            label = my_font.render("НИЧЬЯ", 1, black)
            screen.blit(label, (160, 15))
            self.over = True


class GameApp:
    def __init__(self, board, game, player1, player2):
        self.board = board
        self.game = game
        self.player1 = player1
        self.player2 = player2
        self.square_size = 80
        self.width = columns_count * self.square_size
        self.height = (rows_count + 1) * self.square_size
        pygame.init()
        self.screen = pygame.display.set_mode((self.width, self.height))

    def run(self):
        self.screen.fill(grey)
        self.board.draw_board(self.screen, self.square_size, self.height)
        pygame.display.update()

        while not self.game.over:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.game.turn == 0:
                        self.player1.make_move(event, self.screen)
                    else:
                        self.player2.make_move(event, self.screen)
                    self.board.draw_board(self.screen, self.square_size, self.height)
                    self.game.turn += 1
                    self.game.turn %= 2
                    if self.game.over:
                        pygame.time.wait(3000)


columns_count = 6
rows_count = 6

red = (235, 40, 40)
yellow = (245, 237, 7)
grey = (212, 210, 210)
white = (255, 255, 255)
black = (56, 56, 56)

game_board = Board(columns_count, rows_count)
game_system = Game(game_board)
player_1 = Player(1, red)
player_2 = Player(2, yellow)

app = GameApp(game_board, game_system, player_1, player_2)
app.run()
