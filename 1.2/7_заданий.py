i = (1,15,40)
k = map(lambda a : a*2, i)
print(list(k))

x = map(lambda a,b,c: a*b*c , [2,6,3], [7,4,9], [5,7,10,6])
print(list(x))

a=['anna','alexandra','semen']
k = ( map(lambda x: len(str(x)),a))
print(list(k))

a=[6, 13, 8, 54, 3]
k = (filter(lambda x: x%2==0, a))
print(list(k))

a=['2',' ','44','312']
k=(filter(lambda x: x!=' ',a))
print(list(k))

a=(zip('anna','alexandra','semen'))
print(list(a))

a=(zip('alexandra',map( lambda b: b*2, 'seman')))
print(list(a))
