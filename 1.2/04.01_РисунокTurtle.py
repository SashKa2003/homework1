from turtle import *
color("dim gray")
forward(50)
left(20)
left(90)
forward(50)
left(70)
forward(50)
left(20)
left(90)
forward(50)
reset()
color("dim gray")
begin_fill()
forward(50)
left(110)
forward(50)
left(70)
forward(50)
left(110)
forward(50)
end_fill()
color("gray")
right(40)
begin_fill()
forward(50)
left(110)
forward(10)
left(70)
forward(40)
right(70)
forward(40)
left(70)
forward(10)
left(110)
forward(50)
end_fill()
right(70)
color("dark gray")
begin_fill()
right(110)
forward(50)
right(110)
forward(10)
right(20)
right(90)
left(20)
left(20)
forward(40)
left(70)
forward(40)
right(70)
forward(10)
right(110)
forward(50)
end_fill()
left(40)
color("gray")
forward(50)
left(90)
left(50)
forward(50)
left(40)
forward(10)
color("dim gray")
begin_fill()
forward(40)
left(70)
forward(10)
right(110)
forward(10)
right(70)
forward(10)
right(20)
right(50)
forward(50)
right(110)
forward(10)
end_fill()
left(180)
forward(10)
left(70)
forward(50)
left(40)
forward(50)
left(70)
color("dim gray")
color("dark gray")
begin_fill()
forward(50)
left(70)
forward(40)
left(110)
forward(50)
right(70)
left(140)
forward(40)
end_fill()
right(180)
forward(50)
right(70)
forward(10)
left(70)
forward(50)
right(140)
forward(50)
color("dim gray")
begin_fill()
left(70)
forward(50)
left(110)
forward(50)
left(70)
forward(50)
left(110)
forward(50)
end_fill()
color("dark gray")
right(40)
begin_fill()
forward(50)
left(110)
forward(10)
left(70)
forward(40)
right(70)
forward(40)
forward(10)
left(90)
forward(10)
left(90)
forward(60)
end_fill()
right(180)
forward(60)
right(90)
color("white")
begin_fill()
forward(10)
right(90)
forward(10)
right(110)
forward(10)
right(70)
forward(10)
end_fill()
color("dim gray")
color("white")
left(180)
forward(10)
color("dark gray")
left(70)
forward(10)
color("dim gray")
right(70)
begin_fill()
forward(40)
left(110)
forward(50)
left(70)
forward(50)
left(110)
forward(50)
left(70)
forward(10)
end_fill()
forward(40)
left(70)
right(70)
color("dark gray")
forward(10)
right(110)
forward(10)
color("white")
left(180)
begin_fill()
forward(50)
right(140)
forward(50)
right(40)
forward(50)
right(20)
right(50)
right(70)
forward(50)
end_fill()
right(40)
forward(50)
right(140)
forward(50)
color("dark gray")
left(70)
begin_fill()
forward(10)
left(70)
forward(10)
left(110)
right(110)
forward(40)
left(110)
forward(40)
left(110)
right(110)
forward(2)
left(110)
right(110)
forward(3)
left(110)
forward(50)
left(70)
left(110)
end_fill()
forward(50)
left(70)
forward(10)
right(110)
forward(10)
left(40)
forward(50)
left(140)
begin_fill()
forward(50)
right(70)
forward(50)
right(110)
forward(10)
right(70)
forward(40)
left(70)
forward(40)
right(70)
forward(10)
end_fill()
right(180)
forward(10)
right(110)
forward(10)
left(40)
forward(50)
left(140)
forward(50)
left(40)
forward(50)
right(110)
color("dim gray")
begin_fill()
forward(50)
right(70)
forward(50)
right(110)
forward(50)
right(70)
forward(50)
end_fill()
right(180)
forward(50)
left(70)
forward(50)
right(110)
forward(10)
left(110)
forward(10)
begin_fill()
right(70)
forward(50)
right(110)
forward(50)
right(70)
forward(50)
right(110)
forward(50)
end_fill()
right(70)
forward(50)
right(40)
forward(50)
color("dark gray")
begin_fill()
right(70)
forward(50)
right(110)
forward(50)
right(70)
forward(50)
right(110)
forward(50)
end_fill()
right(70)
forward(50)
right(70)
forward(10)
right(40)
forward(40)
left(40)
forward(40)
right(40)
forward(10)
begin_fill()
right(70)
forward(40)
left(70)
forward(10)
left(110)
forward(50)
left(70)
forward(10)
left(40)
forward(10)
forward(2)
left(140)
forward(12)
end_fill()
color("dim gray")
right(180)
forward(10)
begin_fill()
left(40)
forward(50)
right(180)
forward(10)
left(70)
forward(50)
right(70)
forward(50)
right(110)
forward(50)
right(70)
forward(50)
end_fill()
color("white")
forward(10)
right(180)
forward(10)
color("dark gray")
begin_fill()
left(70)
forward(50)
left(70)
forward(50)
left(110)
forward(50)
left(70)
forward(50)
left(110)
forward(50)
end_fill()
left(70)
forward(50)
right(140)
forward(10)
left(140)
forward(10)
begin_fill()
right(70)
forward(50)
right(110)
forward(50)
right(70)
forward(50)
right(110)
forward(50)
end_fill()
right(70)
forward(50)
right(70)
forward(10)
left(140)
forward(10)
right(70)
begin_fill()
forward(50)
right(110)
forward(50)
right(70)
forward(50)
right(110)
forward(50)
end_fill()
right(70)
forward(50)
right(70)
forward(10)
left(140)
forward(10)
right(70)
begin_fill()
forward(50)
right(110)
forward(50)
right(70)
forward(50)
right(110)
forward(50)
end_fill()
right(70)
forward(50)
right(70)
forward(50)
right(40)
forward(50)
right(140)
forward(50)      
color("dim gray")      
begin_fill()      
left(70)      
left(110)      
forward(50)      
right(110)      
forward(50)      
right(70)      
forward(50)     
right(110)     
forward(50)      
end_fill()      
right(180)      
forward(50)      
right(110)     
forward(40)      
right(40)      
left(80)     
forward(10)      
left(140)      
forward(50)      
left(110)      
right(70)      
forward(50)      
right(110)      
forward(50)      
right(70)      
forward(50)     
begin_fill()      
right(110)      
forward(50)      
right(70)      
forward(50)      
right(110)      
forward(50)      
right(70)     
forward(50)      
end_fill()      
right(40)      
forward(40)      
left(40)      
forward(10)      
left(140)      
forward(50)      
begin_fill()      
left(40)      
forward(50)      
right(110)      
forward(50)      
right(70)      
forward(50)     
right(110)     
forward(50)      
end_fill()      
left(70)      
forward(50)      
right(140)      
forward(10)      
left(140)      
forward(10)      
right(70)      
forward(50)      
right(70)      
forward(10)      
left(140)      
forward(10)      
right(70)      
forward(50)      
right(110)      
forward(50)      
right(70)      
forward(50)      
right(110)      
forward(50)      
left(180)      
forward(10)      
right(140)      
forward(10)
left(140)      
forward(50)      
forward(10)      
forward(50)      
forward(10)     
forward(50)      
begin_fill()      
color("dark gray")      
right(70)      
forward(50)      
right(110)      
forward(50)      
right(70)      
forward(50)      
right(110)      
forward(50)      
end_fill()      
left(40)      
forward(50)      
right(180)     
forward(40)     
right(40)      
forward(10)      
left(110)      
forward(50)      
begin_fill()      
color("White")      
forward(10)      
right(110)      
forward(50)      
right(70)      
forward(10)      
right(110)      
forward(50)      
end_fill()      
left(110)     
color("dark gray")     
forward(50)      
begin_fill()      
left(70)      
forward(50)      
left(110)      
forward(10)      
left(70)      
forward(50)      
end_fill()      
left(110)      
forward(10)      
right(70)      
forward(50)      
begin_fill()      
color("dim gray")      
right(110)      
forward(10)      
right(70)      
forward(40)      
left(70)      
forward(40)      
right(70)      
forward(10)      
right(110)      
forward(50)      
right(70)      
forward(50)      
end_fill()      
left(140)      
forward(50)     
left(40)     
forward(50)      
begin_fill()      
left(70)      
forward(50)      
left(180)      
forward(10)      
left(110)      
forward(10)      
right(110)     
forward(50)      
right(70)      
forward(50)      
right(110)      
forward(10)      
right(70)      
forward(40)     
end_fill()     
color("dark gray")     
left(70)     
forward(40)      
right(70)      
forward(10)      
begin_fill()      
left(40)      
right(80)     
forward(50)    
right(70)     
forward(50)     
right(110)     
forward(50)      
right(70)      
forward(50)      
end_fill()      
right(180)      
forward(50)      
right(70)     
forward(50)      
left(140)      
forward(50)      
left(70)      
forward(50)      
left(180)      
forward(50)      
begin_fill()      
color("white")      
color("dark gray")      
right(40)      
right(20)      
color("white")      
forward(50)      
color("dark gray")      
color("white")      
left(180)     
forward(50)      
color("dark gray")      
left(20)      
left(50)      
right(20)      
forward(50)     
left(180)     
forward(50)      
left(180)      
right(10)      
right(10)      
forward(50)      
color("white")      
left(140)      
forward(50)      
left(40)      
forward(50)      
left(140)      
forward(50)      
left(40)      
forward(50)      
end_fill()      
left(140)      
begin_fill()      
forward(50)      
left(40)      
forward(50)      
left(140)       
forward(50)      
left(40)      
forward(50)       
end_fill()       
color("dark gray")      
left(180)       
forward(180)       
left(180)       
color("white")      
forward(130)       
color("dark gray")      
left(140)      
forward(50)     
