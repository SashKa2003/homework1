import pygame as pg
import random
pg.init()
screen = pg.display.set_mode((300,300))
screen.fill((255, 255, 255))
color = 0
red = (255,0,0)
green = (0, 255, 0)
blue = (0, 0, 255)
pink = (255, 105, 180)
purple = (138, 43, 226)
orange = (255, 140, 0)
yellow = (255, 255, 0)
colors = [red, green, blue, pink, purple, orange, yellow]
clock = pg.time.Clock()
x=10
y=10
running=True
while running:   
    for event in pg.event.get():
        if event.type == pg.QUIT:
            running = False
            pg.quit()
            exit()
        elif event.type == pg.KEYDOWN:
            if event.key == pg.K_w:
                y-=1
            elif event.key == pg.K_a:
                x-=1
            elif event.key == pg.K_s:
                y+=1
            elif event.key == pg.K_c:
                color = random.choice(colors)
            elif event.key == pg.K_d:
                x+=1
    r = pg.Rect(x,y,10,10)
    pg.draw.rect(screen, color, r)

    press_key = pg.key.get_pressed()
    if press_key [pg.K_w]:
        y-=1
    elif press_key [pg.K_a]:
        x-=1
    elif press_key [pg.K_s]:
        y+=1
    elif press_key [pg.K_d]:
        x+=1 
    clock.tick(60)
    pg.display.update() 
