import pygame as pg
pg.init()
screen = pg.display.set_mode((500,500))
running = True 
while running:
    for event in pg.event.get():
        if event.type == pg.KEYDOWN:
            if pg.key.get_pressed()[pg.K_RETURN]:
                print('pressed "Enter"')
            if pg.key.get_pressed()[pg.K_SPACE]:
                print('pressed "Space"')
            if pg.key.get_pressed()[pg.K_w]:
                print('pressed "W"')
            if pg.key.get_pressed()[pg.K_a]: 
                print('pressed "A"')
            if pg.key.get_pressed()[pg.K_s]: 
                print('pressed "S"')
            if pg.key.get_pressed()[pg.K_d]: 
                print('pressed "D"')
            if pg.key.get_pressed()[pg.K_ESCAPE]: 
                print('pressed "ESC"')    
            if pg.key.get_pressed()[pg.K_LEFT]: 
                print('pressed "LEFT"')
            if pg.key.get_pressed()[pg.K_RIGHT]: 
                print('pressed "RIGHT"')
            if pg.key.get_pressed()[pg.K_UP]: 
                print('pressed "UP"')
            if pg.key.get_pressed()[pg.K_DOWN]: 
                print('pressed "DOWN"')
        if event.type == pg.QUIT:
            running = False
pg.quit()
exit()



