from tkinter import *
from tkinter import ttk
from tkinter.ttk import  Radiobutton
from tkinter.filedialog import asksaveasfile
root = Tk()
Choise1_var=StringVar()
Favfilm_var=StringVar()
Age_var=StringVar()
root.title("Анкетирование")
root.geometry('500x500')
frm = ttk.Frame(root, padding=5)
Label(text="Анкета 'Кино'", bg="white", font=('Arial Bold', 20)).place(relx=0.3,y=20)
Label(text="Какой жанр вы предпочитаете?", font=(9)).place(x=10, y=60)
r_var = IntVar(value=-1)
r1 = Radiobutton(text="Комедия",variable=r_var, value=0).place(x=10, y=100)
r2 = Radiobutton(text="Ужасы",variable=r_var, value=1).place(x=100, y=100)
r3 = Radiobutton(text="Фантастика",variable=r_var, value=2).place(x=190, y=100)
r4 = Radiobutton(text="Триллер",variable=r_var, value=3).place(x=290, y=100)
Label(text="Свой вариант:", font=('Arial Bold', 11)).place(x=10, y=125)
choise_label = Entry(root,textvariable = Choise1_var)
choise_label.place(x=140, y=127)
Label(text="Назовите ваш любимый фильм", font=(9)).place(x=10, y=160)
film_label = Entry(root,textvariable = Favfilm_var)
film_label.place(x=320, y=167)
r_var1 = IntVar(value=-1)
Label(text="Как часто вы смотрите фильмы в кинотеатрах?", font=(9)).place(x=10, y=210)
r1 = Radiobutton(text="Раз в неделю",variable=r_var1, value=0).place(x=10, y=250)
r2 = Radiobutton(text="Пару раз в месяц",variable=r_var1, value=1).place(x=105, y=250)
r3 = Radiobutton(text="Раз в 2-3 месяца",variable=r_var1, value=2).place(x=225, y=250)
r4 = Radiobutton(text="Раз в полгода и реже",variable=r_var1, value=3).place(x=340, y=250)
Label(text="Ваш возраст:", font=('Arial Bold',11)).place(x=10, y=280)
age_label = Entry(root,textvariable = Age_var)
age_label.place(x=140, y=280)
def save():
    if r_var.get() == 0:
        choise1 = f"Любимый жанр: Комедия \n"
    elif r_var.get() == 1:
        choise1 = f"Любимый жанр: Ужасы \n"
    elif r_var.get() == 2:
        choise1 = f"Любимый жанр: Фантастика \n"
    elif r_var.get() == 3:
        choise1 = f"Любимый жанр: Триллер \n"
    else:
        choise1= f"Любимый жанр: {Choise1_var.get()} \n"

    if r_var1.get() == 0:
        choise2 = f"Частота просмотра фильма в кинотеатрах: Раз в неделю \n"
    elif r_var1.get() == 1:
        choise2 = f"Частота просмотра фильма в кинотеатрах: Пару раз в месяц \n"
    elif r_var1.get() == 2:
        choise2 = f"Частота просмотра фильма в кинотеатрах: Раз в 2-3 месяца \n"
    elif r_var1.get() == 3:
        choise2 = f"Частота просмотра фильма в кинотеатрах: Раз в полгода и реже \n"
    else:
        choise2= f"Частота просмотра фильма в кинотеатрах: Не указано \n"

    favFilm= f"Любимый фильм: {Favfilm_var.get()} \n"
    age=f"Возраст: {Age_var.get()}"

    with open("file.txt", "w") as f:
        f.writelines([choise1, favFilm, choise2, age])
f = open('file.txt', 'w')
choise1=Choise1_var.get()
f.write(choise1)
f.close()
save_button=Button(root,text = 'Сохранить', command = lambda:save()).place(x=200,y=400)
root.mainloop()
