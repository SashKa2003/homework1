import pygame
import random
 
BLUE = (0, 0, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
FPS = 30
STEP = 2
ANI_PATH = "animations"
 
pygame.init()
game_clock = pygame.time.Clock()
running = True
 
surface = pygame.display.set_mode((500, 500))
surface.fill(WHITE)
my_font = pygame.font.SysFont('Comic Sans MS', 30)
text = my_font.render('You lose!', True, (255, 0, 0))
pygame.display.set_caption('PyGame test')
 
left = []
right = []
stay = pygame.image.load(f'{ANI_PATH}/0.png')
for i in range(1, 6):
    left.append(pygame.image.load(f'{ANI_PATH}/l{i}.png'))
    right.append(pygame.image.load(f'{ANI_PATH}/r{i}.png'))
 
m1 = pygame.sprite.Sprite()
m1.image = stay
m1.rect = m1.image.get_rect()
m1.rect.move_ip(0, 32)
 
def enemy_init():
    enemy = pygame.sprite.Sprite()
    enemy.image = pygame.Surface((10, 10))
    enemy.image.fill('red')
    enemy.rect = enemy.image.get_rect()
    enemy.rect.move_ip(random.randint(50, surface.get_width()),
                       random.randint(surface.get_height(), surface.get_height() + 500))
    return enemy
 
enemies = pygame.sprite.Group()
for _ in range(20):
    enemies.add(enemy_init())
 
anim_num = 0
while running:
    surface.fill(WHITE)
    surface.blit(m1.image, m1.rect)
    for enemy in enemies:
        if enemy.rect.top < 0:
            enemy.kill()
            enemies.add(enemy_init())
        else:
            enemy.rect.move_ip(0, -10)
        surface.blit(enemy.image, enemy.rect)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    keys = pygame.key.get_pressed()
    if pygame.sprite.spritecollideany(m1, enemies):
        surface.blit(text, (50, 50))
    if keys[pygame.K_d] and not keys[pygame.K_a]:
        if m1.rect.right < surface.get_width()-STEP:
            anim_num = (anim_num + 1) % 5
            m1.rect.move_ip(STEP, 0)
            m1.image = right[anim_num]
        else:
            m1.image = stay
    if keys[pygame.K_a] and not keys[pygame.K_d]:
        if m1.rect.left > STEP:
            anim_num = (anim_num + 1) % 5
            m1.rect.move_ip(-2, 0)
            m1.image = left[anim_num]
        else:
            m1.image = stay
    if not keys[pygame.K_a] and not keys[pygame.K_d]:
        anim_num = 0
        m1.image = stay
    pygame.display.update()
    game_clock.tick(FPS)
 
pygame.quit()
